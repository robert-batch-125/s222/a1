let courses = [
	{
		id: "1",
		name: "Electrical",
		description: "Learn about electricity",
		price: 50000,
		isActive: true
	},
	{
		id: "2",
		name: "Civil",
		description: "Learn about building structures",
		price: 45000,
		isActive: true
	},
	{
		id: "3",
		name: "Mechanical",
		description: "Learn about machine structures",
		price: 40000,
		isActive: true
	},
	{
		id: "4",
		name: "Computer",
		description: "Learn about software and hardware",
		price: 55000,
		isActive: true
	}
];


//addCourse

const addCourse = (id, name, description, price, isActive) => {

	courses.push({
		id: id,
		name: name,
		description: description,
		price: price,
		isActive: isActive
	})

alert (`You have created ${name}. It's price is ${price}`);
}


//getsingleCourse
const getSingleCourse = (id) => {

	const findCourse = courses.find((course) =>  course.id === id)
	
	console.log(findCourse);
	return findCourse
}


//getAllCourses
const getAllCourses = () => {

	console.log(courses);

	return courses
}


//Update
const archiveCourse = (index) => {

	console.log(index);
	courses[index].isActive = false
	console.log(courses[index]);
}


const getActive = () => {
	let activeCourses = courses.filter((course) => {
		return course.isActive === true
	})
	console.log(activeCourses)
}


//deleteCourse
const deleteCourse = () => {
	courses.pop()
	return courses
}